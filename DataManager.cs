﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Makator
{
    public static class DataManager
    {
        private const string DbName = "makator";
        private const string PathToData = @"data\";
        private const int MaxSaveLoadAttempts = 10;
        private const int SaveLoadWaitMiliseconds = 1000;
        

        private static readonly List<DbObject> DataCollection = new List<DbObject>();
        private static readonly Dictionary<string, DateTime> LoadedFiles = new Dictionary<string, DateTime>();
        public static DbObject Last;
        private static int SaveLoadAttempts = 0;

        public static DbObject[] GetDataByObdobi(DateTime datumOd, DateTime datumDo)
        {
            LazyLoadNecessaryData(datumOd, datumDo);

            return DataCollection.Where(x => x.Date.Date >= datumOd.Date && x.Date.Date <= datumDo.Date).OrderByDescending(x => x.Date).ToArray();
        }

        private static void LazyLoadNecessaryData(DateTime datumOd, DateTime datumDo)
        {
            var loadingDate = datumDo;
            while (datumOd <= loadingDate)
            {
                var filename = GetFileNameForMonth(loadingDate);
                if (File.Exists(filename))
                {
                    var lastwrite = File.GetLastWriteTime(filename);
                    DateTime lastLoaded;
                    if (!LoadedFiles.TryGetValue(filename, out lastLoaded) || lastLoaded < lastwrite)
                    {
                        Load(filename);
                        
                        if (!LoadedFiles.TryGetValue(filename, out lastLoaded))
                            LoadedFiles.Add(filename, DateTime.Now);
                        else
                            LoadedFiles[filename] = DateTime.Now;
                    }
                }

                loadingDate = loadingDate.AddMonths(-1);
            }
        }

        public static void AddObject(DbObject obj)
        {
            LazyLoadNecessaryData(obj.Date, obj.Date); // Mohly se udát změny
            DataCollection.Add(obj);
            Last = obj;
            Save(true, obj);
        }

        public static void RemoveObject(DbObject obj)
        {
            LazyLoadNecessaryData(obj.Date, obj.Date); // Mohly se udát změny
            DataCollection.Remove(obj); // odebereme

            Save(false, obj);
        }

        private static string GetFileNameForMonth(DateTime datum)
        {
            return string.Format("{3}{0}_{1}_{2}", DbName, datum.Year, datum.Month, PathToData);
        }

        private static void Load(string filename)
        {
            StreamReader file = null;
            try
            {
                SaveLoadAttempts++;
                file = new StreamReader(filename, Encoding.UTF8);
                string radek = null;
                while ((radek = file.ReadLine()) != null)
                {
                    var obj = new DbObject(radek);
                    if (!DataCollection.Contains(obj))
                    {
                        DataCollection.Add(obj);
                    }
                }

                SaveLoadAttempts = 0;
            }
            catch (Exception ex)
            {
                if (SaveLoadAttempts <= MaxSaveLoadAttempts + 1)
                {
                    Thread.Sleep(SaveLoadWaitMiliseconds);
                    Load(filename);
                }
                else
                {
                    if (MessageBox.Show("Došlo k chybě při načítání databáze! " + ex.Message, "Chyba", MessageBoxButtons.RetryCancel) == DialogResult.Retry)
                    {
                        SaveLoadAttempts = 0;
                        Load(filename);
                    }
                }

            }
            finally
            {
                if (file != null)
                    file.Close();
            }
        }

        private static void Save(bool addNew, DbObject obj)
        {
            StreamWriter file = null;

            try
            {
                SaveLoadAttempts++;

                var filename = GetFileNameForMonth(obj.Date);
                Directory.CreateDirectory(PathToData);
                file = new StreamWriter(filename, addNew, Encoding.UTF8);
                if (!addNew) // Removing
                {
                    var dataToSave = DataCollection.Where(x => x.Date.Month == obj.Date.Month &&
                                                                x.Date.Year == obj.Date.Year);

                    foreach (var objToSave in dataToSave)
                    {
                        file.WriteLine(objToSave.ToString());
                    }
                }
                else 
                    file.WriteLine(obj.ToString());

                LoadedFiles.Remove(filename);
                LoadedFiles.Add(filename, DateTime.Now);

                SaveLoadAttempts = 0;
            }
            catch (Exception ex)
            {
                if (SaveLoadAttempts <= MaxSaveLoadAttempts + 1)
                {
                    Thread.Sleep(SaveLoadWaitMiliseconds);
                    Save(addNew, obj);
                }
                else
                {
                    if (MessageBox.Show("Došlo k chybě při ukládání databáze! " + ex.Message, "Chyba",
                            MessageBoxButtons.RetryCancel) == DialogResult.Retry)
                    {
                        SaveLoadAttempts = 0;
                        Save(addNew, obj);
                    }
                }
            }
            finally
            {
                if (file != null)
                    file.Close();
            }
        }
    }
}
