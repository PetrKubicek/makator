﻿using PKForce.Controls;

namespace Makator
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.topPanel = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.timeButton = new System.Windows.Forms.Button();
            this.timeMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.dnesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vceraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.minulyTydenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tentoTydenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.minulyMesicToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tentoMesicToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.tentoRokToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.comboName = new System.Windows.Forms.ComboBox();
            this.datePicker1 = new System.Windows.Forms.DateTimePicker();
            this.datePicker2 = new System.Windows.Forms.DateTimePicker();
            this.bottomPanel = new System.Windows.Forms.Panel();
            this.labelAbout = new System.Windows.Forms.Label();
            this.labelLastRefresh = new System.Windows.Forms.Label();
            this.labelLastAdd = new System.Windows.Forms.Label();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnAddNew = new System.Windows.Forms.Button();
            this.splitPanel = new System.Windows.Forms.SplitContainer();
            this.grid = new System.Windows.Forms.DataGridView();
            this.ClmDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClmCategory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClmName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClmValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.topPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.timeMenu.SuspendLayout();
            this.bottomPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel)).BeginInit();
            this.splitPanel.Panel2.SuspendLayout();
            this.splitPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            this.SuspendLayout();
            // 
            // topPanel
            // 
            this.topPanel.Controls.Add(this.pictureBox1);
            this.topPanel.Controls.Add(this.timeButton);
            this.topPanel.Controls.Add(this.comboName);
            this.topPanel.Controls.Add(this.datePicker1);
            this.topPanel.Controls.Add(this.datePicker2);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(852, 58);
            this.topPanel.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Makator.Properties.Resources.icons8_Pullups_48;
            this.pictureBox1.Location = new System.Drawing.Point(8, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(54, 51);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // timeButton
            // 
            this.timeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.timeButton.ContextMenuStrip = this.timeMenu;
            this.timeButton.Location = new System.Drawing.Point(619, 15);
            this.timeButton.Name = "timeButton";
            this.timeButton.Size = new System.Drawing.Size(24, 23);
            this.timeButton.TabIndex = 3;
            this.timeButton.Text = "->";
            this.timeButton.UseVisualStyleBackColor = true;
            this.timeButton.Click += new System.EventHandler(this.timeButton_Click);
            // 
            // timeMenu
            // 
            this.timeMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dnesToolStripMenuItem,
            this.vceraToolStripMenuItem,
            this.toolStripMenuItem1,
            this.minulyTydenToolStripMenuItem,
            this.tentoTydenToolStripMenuItem,
            this.toolStripMenuItem2,
            this.minulyMesicToolStripMenuItem,
            this.tentoMesicToolStripMenuItem,
            this.toolStripMenuItem3,
            this.tentoRokToolStripMenuItem});
            this.timeMenu.Name = "contextMenuStrip1";
            this.timeMenu.Size = new System.Drawing.Size(153, 198);
            // 
            // dnesToolStripMenuItem
            // 
            this.dnesToolStripMenuItem.Name = "dnesToolStripMenuItem";
            this.dnesToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.dnesToolStripMenuItem.Text = "Dnes";
            this.dnesToolStripMenuItem.Click += new System.EventHandler(this.dnesToolStripMenuItem_Click);
            // 
            // vceraToolStripMenuItem
            // 
            this.vceraToolStripMenuItem.Name = "vceraToolStripMenuItem";
            this.vceraToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.vceraToolStripMenuItem.Text = "Včera";
            this.vceraToolStripMenuItem.Click += new System.EventHandler(this.vceraToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(149, 6);
            // 
            // minulyTydenToolStripMenuItem
            // 
            this.minulyTydenToolStripMenuItem.Name = "minulyTydenToolStripMenuItem";
            this.minulyTydenToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.minulyTydenToolStripMenuItem.Text = "Minulý týden";
            this.minulyTydenToolStripMenuItem.Click += new System.EventHandler(this.minulyTydenToolStripMenuItem_Click);
            // 
            // tentoTydenToolStripMenuItem
            // 
            this.tentoTydenToolStripMenuItem.Name = "tentoTydenToolStripMenuItem";
            this.tentoTydenToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.tentoTydenToolStripMenuItem.Text = "Tento týden";
            this.tentoTydenToolStripMenuItem.Click += new System.EventHandler(this.tentoTydenToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(149, 6);
            // 
            // minulyMesicToolStripMenuItem
            // 
            this.minulyMesicToolStripMenuItem.Name = "minulyMesicToolStripMenuItem";
            this.minulyMesicToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.minulyMesicToolStripMenuItem.Text = "Minulý měsíc";
            this.minulyMesicToolStripMenuItem.Click += new System.EventHandler(this.minulyMesicToolStripMenuItem_Click);
            // 
            // tentoMesicToolStripMenuItem
            // 
            this.tentoMesicToolStripMenuItem.Name = "tentoMesicToolStripMenuItem";
            this.tentoMesicToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.tentoMesicToolStripMenuItem.Text = "Tento měsíc";
            this.tentoMesicToolStripMenuItem.Click += new System.EventHandler(this.tentoMesicToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(149, 6);
            // 
            // tentoRokToolStripMenuItem
            // 
            this.tentoRokToolStripMenuItem.Name = "tentoRokToolStripMenuItem";
            this.tentoRokToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.tentoRokToolStripMenuItem.Text = "Tento rok";
            this.tentoRokToolStripMenuItem.Click += new System.EventHandler(this.tentoRokToolStripMenuItem_Click);
            // 
            // comboName
            // 
            this.comboName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboName.FormattingEnabled = true;
            this.comboName.Location = new System.Drawing.Point(69, 17);
            this.comboName.Name = "comboName";
            this.comboName.Size = new System.Drawing.Size(140, 21);
            this.comboName.TabIndex = 0;
            this.comboName.SelectionChangeCommitted += new System.EventHandler(this.btnRefresh_Click);
            // 
            // datePicker1
            // 
            this.datePicker1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.datePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datePicker1.Location = new System.Drawing.Point(649, 17);
            this.datePicker1.Name = "datePicker1";
            this.datePicker1.Size = new System.Drawing.Size(93, 20);
            this.datePicker1.TabIndex = 1;
            // 
            // datePicker2
            // 
            this.datePicker2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.datePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datePicker2.Location = new System.Drawing.Point(748, 17);
            this.datePicker2.Name = "datePicker2";
            this.datePicker2.Size = new System.Drawing.Size(93, 20);
            this.datePicker2.TabIndex = 2;
            // 
            // bottomPanel
            // 
            this.bottomPanel.Controls.Add(this.labelAbout);
            this.bottomPanel.Controls.Add(this.labelLastRefresh);
            this.bottomPanel.Controls.Add(this.labelLastAdd);
            this.bottomPanel.Controls.Add(this.btnEdit);
            this.bottomPanel.Controls.Add(this.btnRefresh);
            this.bottomPanel.Controls.Add(this.btnDelete);
            this.bottomPanel.Controls.Add(this.btnAddNew);
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomPanel.Location = new System.Drawing.Point(0, 471);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.Size = new System.Drawing.Size(852, 101);
            this.bottomPanel.TabIndex = 0;
            // 
            // labelAbout
            // 
            this.labelAbout.AutoSize = true;
            this.labelAbout.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.25F);
            this.labelAbout.Location = new System.Drawing.Point(12, 79);
            this.labelAbout.Name = "labelAbout";
            this.labelAbout.Size = new System.Drawing.Size(444, 12);
            this.labelAbout.TabIndex = 6;
            this.labelAbout.Text = "Makátor © 2018 Petr Kubíček. Všechna práva vyhrazena. Tento program je freeware. " +
    "www.pkubicek.com.";
            // 
            // labelLastRefresh
            // 
            this.labelLastRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLastRefresh.Location = new System.Drawing.Point(618, 30);
            this.labelLastRefresh.Name = "labelLastRefresh";
            this.labelLastRefresh.Size = new System.Drawing.Size(162, 23);
            this.labelLastRefresh.TabIndex = 5;
            this.labelLastRefresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelLastAdd
            // 
            this.labelLastAdd.Location = new System.Drawing.Point(180, 30);
            this.labelLastAdd.Name = "labelLastAdd";
            this.labelLastAdd.Size = new System.Drawing.Size(129, 23);
            this.labelLastAdd.TabIndex = 4;
            this.labelLastAdd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnEdit
            // 
            this.btnEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEdit.Enabled = false;
            this.btnEdit.Location = new System.Drawing.Point(68, 15);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(50, 50);
            this.btnEdit.TabIndex = 1;
            this.btnEdit.Text = "Upravit";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefresh.Location = new System.Drawing.Point(786, 16);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(54, 50);
            this.btnRefresh.TabIndex = 3;
            this.btnRefresh.Text = "Obnovit";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDelete.Enabled = false;
            this.btnDelete.Location = new System.Drawing.Point(124, 15);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(50, 50);
            this.btnDelete.TabIndex = 2;
            this.btnDelete.Text = "Smazat";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnAddNew
            // 
            this.btnAddNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAddNew.Location = new System.Drawing.Point(12, 15);
            this.btnAddNew.Name = "btnAddNew";
            this.btnAddNew.Size = new System.Drawing.Size(50, 50);
            this.btnAddNew.TabIndex = 0;
            this.btnAddNew.Text = "Přidat";
            this.btnAddNew.UseVisualStyleBackColor = true;
            this.btnAddNew.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // splitPanel
            // 
            this.splitPanel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.splitPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitPanel.CausesValidation = false;
            this.splitPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitPanel.Location = new System.Drawing.Point(0, 58);
            this.splitPanel.Name = "splitPanel";
            // 
            // splitPanel.Panel2
            // 
            this.splitPanel.Panel2.Controls.Add(this.grid);
            this.splitPanel.Size = new System.Drawing.Size(852, 413);
            this.splitPanel.SplitterDistance = 406;
            this.splitPanel.TabIndex = 2;
            this.splitPanel.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitPanel_SplitterMoved);
            this.splitPanel.SizeChanged += new System.EventHandler(this.splitPanel_SizeChanged);
            // 
            // grid
            // 
            this.grid.AllowUserToAddRows = false;
            this.grid.AllowUserToDeleteRows = false;
            this.grid.AllowUserToResizeRows = false;
            this.grid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grid.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.grid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ClmDate,
            this.ClmCategory,
            this.ClmName,
            this.ClmValue});
            this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.grid.Location = new System.Drawing.Point(0, 0);
            this.grid.MultiSelect = false;
            this.grid.Name = "grid";
            this.grid.ReadOnly = true;
            this.grid.RowHeadersVisible = false;
            this.grid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grid.ShowCellErrors = false;
            this.grid.ShowEditingIcon = false;
            this.grid.Size = new System.Drawing.Size(440, 411);
            this.grid.TabIndex = 0;
            this.grid.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grid_CellDoubleClick);
            this.grid.SelectionChanged += new System.EventHandler(this.grid_SelectionChanged);
            this.grid.KeyUp += new System.Windows.Forms.KeyEventHandler(this.grid_KeyUp);
            // 
            // ClmDate
            // 
            this.ClmDate.HeaderText = "Datum";
            this.ClmDate.Name = "ClmDate";
            this.ClmDate.ReadOnly = true;
            // 
            // ClmCategory
            // 
            this.ClmCategory.HeaderText = "Jméno";
            this.ClmCategory.Name = "ClmCategory";
            this.ClmCategory.ReadOnly = true;
            // 
            // ClmName
            // 
            this.ClmName.HeaderText = "Cvik";
            this.ClmName.Name = "ClmName";
            this.ClmName.ReadOnly = true;
            // 
            // ClmValue
            // 
            this.ClmValue.HeaderText = "Hodnota";
            this.ClmValue.Name = "ClmValue";
            this.ClmValue.ReadOnly = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 572);
            this.Controls.Add(this.splitPanel);
            this.Controls.Add(this.bottomPanel);
            this.Controls.Add(this.topPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(640, 480);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Makátor";
            this.topPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.timeMenu.ResumeLayout(false);
            this.bottomPanel.ResumeLayout(false);
            this.bottomPanel.PerformLayout();
            this.splitPanel.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel)).EndInit();
            this.splitPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.DateTimePicker datePicker1;
        private System.Windows.Forms.DateTimePicker datePicker2;
        private System.Windows.Forms.Panel bottomPanel;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnAddNew;
        private System.Windows.Forms.ComboBox comboName;
        private System.Windows.Forms.SplitContainer splitPanel;
        private System.Windows.Forms.DataGridView grid;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClmDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClmCategory;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClmName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClmValue;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button timeButton;
        private System.Windows.Forms.ContextMenuStrip timeMenu;
        private System.Windows.Forms.ToolStripMenuItem dnesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vceraToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem minulyTydenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tentoTydenToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem minulyMesicToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tentoMesicToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem tentoRokToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label labelLastRefresh;
        private System.Windows.Forms.Label labelLastAdd;
        private System.Windows.Forms.Label labelAbout;
    }
}

