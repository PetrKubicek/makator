﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;

namespace Makator
{
    public static class Strings
    {
        private static readonly Random Random = new Random(DateTime.Now.Millisecond);

        public static string ProductVersion => FileVersionInfo.GetVersionInfo(Assembly.GetCallingAssembly().Location).ProductVersion;


        public static string GetNoDataText()
        {
            return "Asi na to všichni kašlou... Žádná data. Čas na motivační citát:";
        }

        public static string GetOstatniText()
        {
            var list = new List<string>
            {
                "Ostatní loseři",
                "Ten zbytek",
                "Ostatní plevel",
                "Další nemakčenka",
                "Nějací další cucáci",
                "Slabší jedinci",
                "Ostatní chcípáci",
            };

            var randomNumber = Random.Next(list.Count);
            return list[randomNumber];
        }

        public static string GetMotivacniCitat()
        {
            var list = new List<string>
            {
                "„Síla nepochází z fyzických schopností, ale z nezlomné vůle.“ — Mahátma Gándhí",
                "„Jestli chceš, abych něco udělal, řekni mi, že to nezvládnu.“ — Maya ",
                "„Cílová čára je pouze začátek úplně nového závodu.“ — Neznámý autor",
                "„Motivace je to, díky čemuž začnete. Zvyk je to, díky čemuž v tom budete pokračovat.“ — Jim Ryan",
                "„Nic velkého nebylo nikdy dosaženo bez nadšení.“ — Ralph Waldo Emerson",
                "„Je to jednoduché. Pokud se to třese, je to tuk.“ — Arnold Schwarzenegger",
                "„Život je pro mě být neustále hladový. Význam života není pouze existovat, přežít, ale jít kupředu, stoupat nahoru, dosáhnout úspěchů, zvítězit.“ — Arnold Schwarzenegger",
                "„Obtížnost je výmluva, kterou historie nikdy nepřijímá.“ — Edward R. Murrow",
                "„Zlepšení může přijít, až když přijmeme své vlastní slabiny.“ — Jean Vanier",
                "„Ten, který říká, že to zvládne a ten, který říká, že to nezvládne, mají oba pravdu.“ — Confusius",
                "„Snažte se dosáhnout zlepšení, ne dokonalosti.“ — Neznámý autor",
                "„Jestliže neděláte žádné chyby, nejspíš se doopravdy ani nesnažíte.“ — Neznámý autor",
                "„I ten největší strom byl kdysi jen semínkem v zemi.“ — Neznámý autor",
                "„Jestli je to pro vás důležité, tak si najdete způsob jak toho dosáhnout. Jestli ne, tak si najdete výmluvu.“ — Jim Rohn",
                "„Když budete stále dělat, co jste vždycky dělali, dostanete pouze to, co jste vždycky dostávali.“ — Susan Jeffries",
                "„Bolest je dočasná, hrdost je věčná.“ — Neznámý autor",
                "„Trp bolestí z disciplíny nebo trp bolestí z výčitek!“ — Neznámý autor",
                "„Co tě nezabije, to tě posílí.“ — Friedrich Nietzsche",
                "„Upadni sedmkrát, vstaň osmkrát.“ — Čínské přísloví",
                "„Na cestě k úspěchu je mnoho překážek. Proto se snažte, abyste se nestali jednou z nich.“ — Ralph Marston",
                "„Síla je produktem snahy.“ — Neznámý autor",
                "„Být poražený je často jen dočasný stav. Trvalé to je, až když se vzdáte.“ — George Elliot",
                "„Udělat první krok – to je to, co odlišuje vítěze od poražených.“ — Brian Tracy",
                "„Neexistují žádná tajemství o úspěchu. Je to výsledek přípravy, tvrdé práce a učení se z chyb.“ — Colin Powell",
                "„Nenáviděl jsem každou minutu tréninku, ale vždy jsem si řekl: Neskončíš, trp teď a žij zbytek života jako šampión.“ — Muhammad Ali",
                "„Být zdravý a fit není hloupý nápad nebo trend, ale životní styl.“ ",
                "„Není to lehčí, to jen vy jste silnější.“ — Neznámý autor",
            };

            var randomNumber = Random.Next(list.Count);
            return list[randomNumber];
        }

        public static string GetMasterText()
        {
            var list = new List<string>
            {
                "King of the hill",
                "Superborec",
                "Král",
                "Hustokrutor",
                "Mega master",
                "Fitness führer",
                "Druhej po Chucku Norrisovi",
                "Master blaster",
                "Superfrajer",
                "Kruťák největší",
                "Terminator",
                "Demoliton man",
                "Největší zabiják",
                "Big Boss",
                "Nezná bolest",
            };

            var randomNumber = Random.Next(list.Count);
            return list[randomNumber];
        }

        public static string GetCopyrightText()
        {
            return string.Format("Makátor verze {0} © 2017-{1} Petr Kubíček. Všechna práva vyhrazena. Tento program je freeware. www.pkubicek.com.", ProductVersion, DateTime.Now.Year);
        }
    }
}
