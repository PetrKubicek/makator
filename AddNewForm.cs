﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace Makator
{
    public partial class AddNewForm : Form
    {
        public DbObject Result = new DbObject();

        public AddNewForm(string thisName, DbObject old = null)
        {
            InitializeComponent();

            var data = DataManager.GetDataByObdobi(DateTime.Now.AddDays(-14), DateTime.Now);
            var mostVals = data.GroupBy(x => x.Value).OrderByDescending(x => x.Count()).Take(7).Select(x => x.Key.ToString()).ToArray();
            var categories = data.Select(x => x.Category).Distinct().ToArray();
            var names = data.Select(x => x.Name).Distinct().ToArray();

            comboCategory.Items.AddRange(categories);
            comboName.Items.AddRange(names);
            if (DataManager.Last != null)
            {
                comboCategory.Text = DataManager.Last.Category;
                boxValue.Value = DataManager.Last.Value;
            }

            comboName.Text = thisName;
            datePicker.Value = DateTime.Now;

            foreach (var mostVal in mostVals)
            {
                var newBut = new Button()
                {
                    Text = mostVal,
                    Width = 50,
                    Height = 50,
                };
                newBut.Click += (sender, args) => { boxValue.Text = (sender as Button).Text; };

                flowPanel.Controls.Add(newBut);
            }

            if (old != null)
            {
                datePicker.Value = old.Date;
                comboCategory.Text = old.Category;
                comboName.Text = old.Name;
                boxValue.Text = old.Value.ToString();
            }
        }

        private void AddNewForm_Load(object sender, EventArgs e)
        {

        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Result = new DbObject()
            {
                Date = datePicker.Value,
                Category = comboCategory.Text,
                Name = comboName.Text,
                Value = int.Parse(boxValue.Text),
                Memo = null,
            };

            this.Close();
        }
        private void boxValue_Enter(object sender, EventArgs e)
        {
            boxValue.Text = string.Empty;
        }

        private void boxValue_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(boxValue.Text))
                boxValue.Text = "1";
        }
    }
}
