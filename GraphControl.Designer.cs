﻿namespace Makator
{
    partial class GraphControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelName = new System.Windows.Forms.Label();
            this.labelVal = new System.Windows.Forms.Label();
            this.val = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // labelName
            // 
            this.labelName.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelName.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.labelName.Location = new System.Drawing.Point(0, 0);
            this.labelName.Name = "labelName";
            this.labelName.Padding = new System.Windows.Forms.Padding(2);
            this.labelName.Size = new System.Drawing.Size(80, 26);
            this.labelName.TabIndex = 1;
            this.labelName.Text = "label1";
            this.labelName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelVal
            // 
            this.labelVal.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelVal.Location = new System.Drawing.Point(443, 0);
            this.labelVal.Name = "labelVal";
            this.labelVal.Padding = new System.Windows.Forms.Padding(2);
            this.labelVal.Size = new System.Drawing.Size(45, 26);
            this.labelVal.TabIndex = 2;
            this.labelVal.Text = "9999";
            this.labelVal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // val
            // 
            this.val.BackColor = System.Drawing.SystemColors.Control;
            this.val.Dock = System.Windows.Forms.DockStyle.Fill;
            this.val.Location = new System.Drawing.Point(80, 0);
            this.val.Name = "val";
            this.val.Size = new System.Drawing.Size(363, 26);
            this.val.Step = 1;
            this.val.TabIndex = 3;
            // 
            // GraphControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.val);
            this.Controls.Add(this.labelVal);
            this.Controls.Add(this.labelName);
            this.Name = "GraphControl";
            this.Size = new System.Drawing.Size(488, 26);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelVal;
        private System.Windows.Forms.ProgressBar val;
    }
}
