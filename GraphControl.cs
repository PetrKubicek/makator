﻿using System.Windows.Forms;

namespace Makator
{
    public partial class GraphControl : UserControl
    {
        public int Maximum
        {
            get { return val.Maximum; } 
            set { val.Maximum = value; }
        }

        public int Minimum
        {
            get { return val.Minimum; }
            set { val.Minimum = value; }
        }

        public int Value
        {
            get { return val.Value; }
            set
            {
                val.Value = value;
                labelVal.Text = value.ToString();
            }
        }

        public string Name
        {
            get { return labelName.Text; }
            set { labelName.Text = value; }
        }

        public GraphControl()
        {
            InitializeComponent();
        }
    }
}
