﻿using System;
using System.Globalization;

namespace Makator
{
    public class DbObject
    {
        private const char Splitter = ';';

        public DateTime Date { get; set; }
        public string Category { get; set; }
        public string Name { get; set; }
        public int Value { get; set; }
        public string Memo { get; set; }

        public DbObject()
        {
        }

        /// <summary>
        /// Date, Category, Name, Value, Memo
        /// </summary>
        public DbObject(string csvLine)
        {
            string[] vals = csvLine.Split(Splitter);
            if (vals.Length != 5)
                return;

            Date = DateTime.ParseExact(vals[0], "yyyy-dd-MM HH:mm:ss", CultureInfo.InvariantCulture);
            Category = vals[1];
            Name = vals[2];
            Value = int.Parse(vals[3]);
            Memo = vals[4];
        }

        public override string ToString()
        {
            return Date.ToString("yyyy-dd-MM HH:mm:ss", CultureInfo.InvariantCulture) + Splitter +
                   Category + Splitter +
                   Name + Splitter +
                   Value + Splitter +
                   Memo;
        }

        public override bool Equals(object obj)
        {
            return this.ToString() == obj.ToString();
        }
    }
}
