﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using PKForce.Controls;

namespace Makator
{
    public partial class MainForm : Form
    {
        private DbObject[] _data;
        private string[] _names;

        public MainForm()
        {
            InitializeComponent();

            labelAbout.Text = Strings.GetCopyrightText();
            tentoMesicToolStripMenuItem_Click(null, null);
            btnRefresh_Click(null, null);

            datePicker1.ValueChanged += btnRefresh_Click;
            datePicker2.ValueChanged += btnRefresh_Click;
        }

        private void ProcessASync(DoWorkEventHandler hardWorkHandler, RunWorkerCompletedEventHandler onCompleted = null)
        {
            bottomPanel.Enabled = false;
            // Vytvoření indikátoru
            KProgress indikator = new KProgress();
            indikator.Dock = DockStyle.Fill;
            indikator.Polomer = 30;
            this.Controls.Add(indikator);
            indikator.BringToFront();

            // Uložení databáze v samostatném vláknu
            BackgroundWorker vlakno = new BackgroundWorker();
            vlakno.DoWork += hardWorkHandler;
            // Po skončení 
            vlakno.RunWorkerCompleted += delegate
            {
                this.Controls.Remove(indikator);
                indikator.Dispose();
                bottomPanel.Enabled = true;
            };
            vlakno.RunWorkerCompleted += onCompleted;

            vlakno.RunWorkerAsync();
        }

        private void RefreshData()
        {
            ProcessASync((sender, args) =>
            {
                _data = DataManager.GetDataByObdobi(datePicker1.Value, datePicker2.Value);
                _names = _data.Select(x => x.Name).Distinct().ToArray();
            }, (sender, args) =>
            {
                RefreshNames();

                var selName = comboName.Text;
                var dataToShow = _data.Where(x => x.Name == selName).ToArray();

                RefreshGrid(dataToShow);

                splitPanel.Panel1.Controls.Clear();
                var groupedDataToShow = dataToShow.GroupBy(x => x.Category).OrderByDescending(x => x.Sum(y => y.Value));

                if (!groupedDataToShow.Any())
                {
                    ShowMotivacniCitat();
                }
                else
                    ShowStats(groupedDataToShow);

                labelLastRefresh.Text = string.Format("Naposledy obnoveno: {0}", DateTime.Now.ToShortTimeString());
            });
        }

        private void ShowStats(IOrderedEnumerable<IGrouping<string, DbObject>> grouped)
        {
            var max = grouped.First().Sum(x => x.Value);
            var min = 0;

            for (int i = 0; i < grouped.Count(); i++)
            {
                var group = grouped.ElementAt(i);
                var top = i + 2;
                if (i > 0)
                    top++;


                if (i == 0)
                {
                    var label = new Label()
                    {
                        Top = 22,
                        Font = new Font(DefaultFont, FontStyle.Bold),
                        Text = Strings.GetMasterText(),
                        Width = splitPanel.Panel1.Width,
                    };

                    splitPanel.Panel1.Controls.Add(label);
                }

                if (i == 1)
                {
                    var label = new Label()
                    {
                        Font = new Font(DefaultFont, FontStyle.Bold),
                        Top = 25 * 3,
                        Text = Strings.GetOstatniText(),
                        Width = splitPanel.Panel1.Width,
                    };

                    splitPanel.Panel1.Controls.Add(label);
                }

                var graph = new GraphControl()
                {
                    Name = group.Key,
                    Maximum = max,
                    Minimum = min,
                    Value = group.Sum(x => x.Value),
                    Height = 20,
                    Top = 23 * (top),
                    Width = splitPanel.Panel1.Width,
                };

                if (i >= 1)
                    graph.Top += 6;

                splitPanel.Panel1.Controls.Add(graph);
            }
        }

        private void ShowMotivacniCitat()
        {
            var label = new Label()
            {
                Top = 22,
                Font = new Font(DefaultFont, FontStyle.Bold),
                Text = Strings.GetNoDataText(),
                Width = splitPanel.Panel1.Width,
            };

            var label2 = new Label()
            {
                Top = 48,
                Text = Strings.GetMotivacniCitat(),
                Width = splitPanel.Panel1.Width,
                Height = 200
            };

            splitPanel.Panel1.Controls.Add(label);
            splitPanel.Panel1.Controls.Add(label2);
        }

        private void RefreshGrid(DbObject[] toShow)
        {
            grid.Rows.Clear();
            foreach (var obj in toShow)
            {
                var row = new DataGridViewRow();
                var cells = new object[]
                {
                        obj.Date, obj.Category, obj.Name, obj.Value
                };

                row.CreateCells(grid, cells);
                row.Tag = obj;

                grid.Rows.Add(row);
            }
        }

        private void RefreshNames()
        {
            if (_names.Length > 0)
            {
                var index = comboName.SelectedIndex;
                comboName.Items.Clear();
                comboName.Items.AddRange(_names);
                if (index < comboName.Items.Count && index >= 0)
                    comboName.SelectedIndex = index;
                else
                    comboName.SelectedIndex = 0;
            }
        }

        #region buttons

        private void btnAddNew_Click(object sender, EventArgs e)
        {
          
            var addDialog = new AddNewForm(comboName.Text);
            if (addDialog.ShowDialog() == DialogResult.OK)
            {
                ProcessASync((o, args) => DataManager.AddObject(addDialog.Result), (o, args) => RefreshData());
                labelLastAdd.Text = string.Format("Poslední přidání: {0}", DateTime.Now.ToShortTimeString());
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (grid.SelectedRows.Count <= 0)
                return;

            var row = grid.SelectedRows[0];
            var obj = row.Tag as DbObject;
            if (obj == null)
                return;

            if (MessageBox.Show("Opravdu smazat?", "Smazat", MessageBoxButtons.YesNo) == DialogResult.Yes)
                ProcessASync((o, args) => DataManager.RemoveObject(obj), (o, args) => RefreshData());
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            RefreshData();
        }
        
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (grid.SelectedRows.Count <= 0)
                return;

            var row = grid.SelectedRows[0];
            var obj = row.Tag as DbObject;
            if (obj == null)
                return;

            var addDialog = new AddNewForm(comboName.Text, obj);
            if (addDialog.ShowDialog() == DialogResult.OK)
            {
                ProcessASync((o, args) => DataManager.RemoveObject(obj), 
                             (o, args) => ProcessASync((x, y) => DataManager.AddObject(addDialog.Result), (x, y) => RefreshData()));

            }
        }

        #endregion

        #region splitPanel

        private void splitPanel_SizeChanged(object sender, EventArgs e)
        {
            foreach (Control ctrl in splitPanel.Panel1.Controls)
            {
                ctrl.Width = splitPanel.Panel1.Width;
            }
        }

        private void splitPanel_SplitterMoved(object sender, SplitterEventArgs e)
        {
            splitPanel_SizeChanged(sender, e);
        }

        #endregion

        #region timeMenu clicks

        private void timeButton_Click(object sender, EventArgs e)
        {
            timeMenu.Show(timeButton, 0, 30);
        }

        private void dnesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            datePicker1.ValueChanged -= btnRefresh_Click;

            datePicker1.Value = 
            datePicker2.Value = DateTime.Today;

            datePicker1.ValueChanged += btnRefresh_Click;
        }

        private void vceraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            datePicker1.ValueChanged -= btnRefresh_Click;

            datePicker1.Value = 
            datePicker2.Value = DateTime.Today.AddDays(-1);

            datePicker1.ValueChanged += btnRefresh_Click;
        }

        private void minulyTydenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            datePicker1.ValueChanged -= btnRefresh_Click;

            var input = DateTime.Now.AddDays(-7);
            int delta = DayOfWeek.Monday - input.DayOfWeek;
            DateTime monday = input.AddDays(delta);

            datePicker1.Value = monday;
            datePicker2.Value = monday.AddDays(6);

            datePicker1.ValueChanged += btnRefresh_Click;
        }

        private void tentoTydenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            datePicker1.ValueChanged -= btnRefresh_Click;

            int delta = DayOfWeek.Monday - DateTime.Now.DayOfWeek;
            DateTime monday = DateTime.Now.AddDays(delta);

            datePicker1.Value = monday;
            datePicker2.Value = monday.AddDays(6);

            datePicker1.ValueChanged += btnRefresh_Click;
        }

        private void minulyMesicToolStripMenuItem_Click(object sender, EventArgs e)
        {
            datePicker1.ValueChanged -= btnRefresh_Click;

            var lastDate = DateTime.Today.AddMonths(-1);
            var date = new DateTime(lastDate.Year, lastDate.Month, 1);
            datePicker1.Value = date;
            datePicker2.Value = date.AddMonths(1).AddDays(-1);

            datePicker1.ValueChanged += btnRefresh_Click;
        }

        private void tentoMesicToolStripMenuItem_Click(object sender, EventArgs e)
        {
            datePicker1.ValueChanged -= btnRefresh_Click;

            var date = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
            datePicker1.Value = date;
            datePicker2.Value = date.AddMonths(1).AddDays(-1);

            datePicker1.ValueChanged += btnRefresh_Click;
        }

        private void tentoRokToolStripMenuItem_Click(object sender, EventArgs e)
        {
            datePicker1.ValueChanged -= btnRefresh_Click;

            var date = new DateTime(DateTime.Today.Year, 1, 1);
            datePicker1.Value = date;
            datePicker2.Value = date.AddYears(1).AddDays(-1);

            datePicker1.ValueChanged += btnRefresh_Click;
        }

        #endregion

        #region grid

        private void grid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            btnEdit_Click(sender, e);
        }

        private void grid_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Delete && btnDelete.Enabled)
                btnDelete_Click(sender, e);
        }

        private void grid_SelectionChanged(object sender, EventArgs e)
        {
            var enabled = (grid.SelectedRows.Count > 0);
            btnDelete.Enabled = btnEdit.Enabled = enabled;
        }

        #endregion
    }
}
